package com.page.java.fsm;

public enum EntranceMachineState {
    LOCKED,
    UNLOCKED
}
